// EX1:Quản lý tuyển sinh

function khuVuc(location) {
  var diemVung = 0;
  if (location == "a") {
    diemVung = 2;
  } else if (location == "b") {
    diemVung = 1;
  } else if (location == "ko") {
    diemVung = 0;
  } else {
    diemVung = 0.5;
  }
  return diemVung;
}

function doiTuong(person) {
  switch (person) {
    case "1": {
      return 2.5;
    }
    case "2": {
      return 1.5;
    }
    case "3": {
      return 1;
    }
    case "0": {
      return 0;
    }
  }
}

function ketQua() {
  var chonKhuvuc = nhapSelect("selection");
  var chonDoiTuong = nhapSelect("selection1");

  var diemKhuVuc = khuVuc(chonKhuvuc) * 1;
  var diemDoiTuong = doiTuong(chonDoiTuong) * 1;

  var diemChuan = nhapSelect("diem_chuan") * 1;
  var diemMon1 = nhapSelect("diem_mon_1") * 1;
  var diemMon2 = nhapSelect("diem_mon_2") * 1;
  var diemMon3 = nhapSelect("diem_mon_3") * 1;

  var tongDiem = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;

  if (tongDiem >= diemChuan) {
    if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
      document.getElementById(
        "ketQua"
      ).innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0!`;
    } else {
      document.getElementById(
        "ketQua"
      ).innerHTML = `Bạn đã đậu. Tổng điểm của bạn là ${tongDiem}!`;
    }
  } else {
    if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
      document.getElementById(
        "ketQua"
      ).innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0!`;
    } else {
      document.getElementById(
        "ketQua"
      ).innerHTML = `Bạn đã rớt. Tổng điểm của bạn là ${tongDiem}!`;
    }
  }
}

function nhapSelect(id) {
  return document.getElementById(id).value;
}

// EX2:Tính tiền điện

function tinhKW(elect) {
  var unit = 0;
  if (elect <= 0) {
    alert("Dữ liệu không hợp lệ, vui lòng nhập lại");
  } else if (elect <= 50) {
    unit = elect * 500;
  } else if (elect <= 100) {
    unit = 500 * 50 + (elect - 50) * 650;
  } else if (elect <= 200) {
    unit = 500 * 50 + 650 * 50 + (elect - 100) * 850;
  } else if (elect <= 350) {
    unit = 500 * 50 + 650 * 50 + 100 * 850 + (elect - 200) * 1100;
  } else {
    unit = 500 * 50 + 650 * 50 + 100 * 850 + 150 * 1100 + (elect - 350) * 1300;
  }
  return unit;
}

// main function
function tinhTienDien() {
  var nhapKW = nhapSelect("nhapKW");
  var soTienDien = tinhKW(nhapKW);
  var nhapHoTen = nhapSelect("nhapHoTen");
  document.getElementById("tinhTienDien").innerHTML =
    "Họ tên: " +
    nhapHoTen +
    "; " +
    "Tiền điện: " +
    new Intl.NumberFormat("vn-VN").format(soTienDien);
}
